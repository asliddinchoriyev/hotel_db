package uz.pdp.hotel_crud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import uz.pdp.hotel_crud.entity.Hotel;
import uz.pdp.hotel_crud.entity.Room;
import uz.pdp.hotel_crud.payload.RoomDto;
import uz.pdp.hotel_crud.repository.HotelRepository;
import uz.pdp.hotel_crud.repository.RoomRepository;

import java.util.Optional;

@RestController
@RequestMapping(value = "/room")
public class RoomController {

    @Autowired
    HotelRepository hotelRepository;

    @Autowired
    RoomRepository roomRepository;

    @PostMapping
    public String add(@RequestBody RoomDto roomDto) {

        Optional<Hotel> optionalHotel = hotelRepository.findById(roomDto.getHotelId());
        if (optionalHotel.isEmpty())
            return "Hotel not found";

        boolean existsRoomByNumberInHotel = roomRepository.existsByNumberAndAndHotelAndIdNot(
                roomDto.getNumber(), optionalHotel.get(), roomDto.getHotelId());
        if (!existsRoomByNumberInHotel)
            return "This room already available";

        Room room = new Room();
        room.setHotel(optionalHotel.get());
        room.setNumber(roomDto.getNumber());
        room.setFloor(roomDto.getFloor());
        room.setSize(roomDto.getSize());

        roomRepository.save(room);

        return "Hotel saved";
    }

    @GetMapping
    public Page<Room> getAll(Integer page) {
        Pageable pageable = PageRequest.of(page, 2);
        return roomRepository.findAll(pageable);
    }

    @GetMapping(value = "/{id}")
    public Room get(@PathVariable Integer id) {
        Optional<Room> optionalRoom = roomRepository.findById(id);

        return optionalRoom.orElseGet(Room::new);

    }

    @GetMapping(value = "/forHotelOwner/{hotelId}")
    public Page<Room> getRoomsByHotelId(@PathVariable Integer hotelId,
                                        @RequestParam int page) {
        Pageable pageable = PageRequest.of(page, 2);

        Page<Room> allByHotelId = roomRepository.findAllByHotel_Id(hotelId, pageable);

        return allByHotelId;
    }

    @DeleteMapping(value = "/{id}")
    public String delete(@PathVariable Integer id) {
        boolean existsById = roomRepository.existsById(id);

        if (existsById) {
            roomRepository.deleteById(id);
            return "Room deleted";
        } else
            return "Room not found";
    }

    @PutMapping(value = "/{id}")
    public String edit(@RequestBody RoomDto roomDto, @PathVariable Integer id) {
        Optional<Room> optionalRoom = roomRepository.findById(id);

        if (optionalRoom.isEmpty())
            return "Room not found";

        Optional<Hotel> optionalHotel = hotelRepository.findById(roomDto.getHotelId());
        if (optionalHotel.isEmpty())
            return "Hotel not found";

        boolean existsRoomByNumber = roomRepository.existsByNumberAndAndHotelAndIdNot(
                roomDto.getNumber(), optionalHotel.get(), roomDto.getHotelId());

        if (existsRoomByNumber)
            return "This room already available";

        Room room = optionalRoom.get();
        room.setHotel(optionalHotel.get());
        room.setNumber(roomDto.getNumber());
        room.setFloor(roomDto.getFloor());
        room.setSize(roomDto.getSize());

        roomRepository.save(room);

        return "Hotel edited";
    }
}
