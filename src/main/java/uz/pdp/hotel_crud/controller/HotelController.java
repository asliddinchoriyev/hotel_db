package uz.pdp.hotel_crud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import uz.pdp.hotel_crud.entity.Hotel;
import uz.pdp.hotel_crud.repository.HotelRepository;

import java.util.Optional;

@RestController
@RequestMapping(value = "/hotel")
public class HotelController {
    @Autowired
    HotelRepository hotelRepository;

    @PostMapping
    public String add(@RequestBody Hotel hotel) {

        boolean exists = hotelRepository.existsHotelByName(hotel.getName());
        if (exists) {
            return "This hotel already added";
        }

        hotelRepository.save(hotel);
        return "Hotel saved";
    }

    @GetMapping
    public Page<Hotel> getAll(Integer page) {
        Pageable pageable = PageRequest.of(page, 2);

        return hotelRepository.findAll(pageable);
    }


    @GetMapping(value = "/{id}")
    public Hotel get(@PathVariable Integer id) {
        Optional<Hotel> optionalHotel = hotelRepository.findById(id);

        return optionalHotel.orElseGet(Hotel::new);

    }

    @DeleteMapping(value = "/{id}")
    public String delete(@PathVariable Integer id) {
        boolean existsById = hotelRepository.existsById(id);

        if (existsById) {
            hotelRepository.deleteById(id);
            return "Hotel deleted";
        } else
            return "Hotel not found";
    }

    @PutMapping(value = "/{id}")
    public String edit(@RequestBody Hotel comingHotel, @PathVariable Integer id) {
        Optional<Hotel> optionalRegion = hotelRepository.findById(id);

        if (optionalRegion.isEmpty())
            return "Hotel not found";

        boolean existsHotelByName = hotelRepository.existsHotelByName(comingHotel.getName());
        if (existsHotelByName)
            return "This hotel already added";

        Hotel hotel = optionalRegion.get();
        hotel.setName(comingHotel.getName());
        hotelRepository.save(hotel);

        return "Hotel edited";
    }


}

