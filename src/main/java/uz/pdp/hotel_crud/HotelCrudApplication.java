package uz.pdp.hotel_crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(HotelCrudApplication.class, args);
    }

}
