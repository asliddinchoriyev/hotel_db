package uz.pdp.hotel_crud.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.hotel_crud.entity.Hotel;
import uz.pdp.hotel_crud.entity.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    Page<Room> findAllByHotel_Id(Integer hotel_id, Pageable pageable);

    boolean existsByNumberAndAndHotelAndIdNot(Integer number, Hotel hotel, Integer id);
}
